unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, GoogleAuthenticator, ClipBrd, IniFiles, IdCoder, IdCoderMIME,
  Vcl.AppEvnts;

type
  TfrmMain = class(TForm)
    edtSerial: TEdit;
    tAuth: TTimer;
    btnOk: TButton;
    edtPassword: TEdit;
    edtToken: TEdit;
    appevMain: TApplicationEvents;
    procedure tAuthTimer(Sender: TObject);
    procedure edtSerialChange(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edtPasswordChange(Sender: TObject);
    procedure edtTokenClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure appevMainShortCut(var Msg: TWMKey; var Handled: Boolean);
  private
    { Private declarations }
    procedure EnableAuth();
    function EnDeCrypt(const Value : String) : String;
    function Encode(const Value : String) : String;
    function Decode(const Value : String) : String;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  iniFile: TIniFile;
const
  iniFilePath = '.\GAuth.ini';
  authSection = 'AuthInfo';
  passwordKey =  'PASSWORD';
  serialKey   = 'SERIAL';

implementation

{$R *.dfm}

function TfrmMain.EnDeCrypt(const Value : String) : String;
var
  CharIndex : integer;
begin
  Result := Value;
  for CharIndex := 1 to Length(Value) do
    Result[CharIndex] := chr(not(ord(Value[CharIndex])));
end;

function TfrmMain.Encode(const Value : String) : String;
begin
  Result := '';//TIdEncoderMIME.EncodeBytes(TEncoding.UTF8.GetBytes(Value));//FIXIT
end;

function TfrmMain.Decode(const Value : String) : String;
begin
  Result := '';//TEncoding.UTF8.GetString(TIdDecoderMIME.DecodeBytes(Value));
end;

procedure TfrmMain.EnableAuth();
begin
  tAuth.Enabled := False;
  btnOk.Enabled := True;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  if(FileExists(iniFilePath))then
  begin
    iniFile := TIniFile.Create(iniFilePath);
    edtPassword.Text := iniFile.ReadString(authSection,passwordKey, edtPassword.Text );
    edtSerial.Text := iniFile.ReadString(authSection,serialKey, edtSerial.Text);
  end
  else
  begin
   iniFile := TIniFile.Create(iniFilePath);
   iniFile.WriteString(authSection,passwordKey,'');
   iniFile.WriteString(authSection,serialKey,'');
   iniFile.Free;
  end;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
   iniFile := TIniFile.Create(iniFilePath);
   iniFile.WriteString(authSection,passwordKey,edtPassword.Text);
   iniFile.WriteString(authSection,serialKey,edtSerial.Text);
   iniFile.Free;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  btnOk.Enabled := False;
  tAuth.Enabled := True;
  tAuth.OnTimer(Self);
end;

procedure TfrmMain.appevMainShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  case Msg.CharCode of Ord('C'), Ord('T'):
    if GetKeyState(VK_Control) < 0 then
    begin  // Control key is down
      Clipboard.AsText := edtToken.Text;
    end;
  end;
end;

procedure TfrmMain.btnOkClick(Sender: TObject);
begin
  tAuth.Enabled := true;
  tAuth.OnTimer(Self);
  btnOk.Enabled := False;
end;

procedure TfrmMain.edtPasswordChange(Sender: TObject);
begin
  EnableAuth();
end;

procedure TfrmMain.edtSerialChange(Sender: TObject);
begin
  EnableAuth();
end;

procedure TfrmMain.edtTokenClick(Sender: TObject);
begin
  if(edtToken.Text <> EmptyStr)then
  begin
    edtToken.SelectAll;
    Clipboard.AsText := edtToken.Text;
    ShowMessage('Text has been copied to clipboard!');
  end;
end;

procedure TfrmMain.tAuthTimer(Sender: TObject);
begin
  if(edtSerial.Text <> EmptyStr)then
  begin
    if(edtPassword.Text <> EmptyStr)then
    begin
     edtToken.Text := edtPassword.Text;
    end
    else
    begin
      edtToken.Text := EmptyStr;
    end;
    edtToken.Text := edtToken.Text + GoogleAuthenticatorCode(edtSerial.Text);
  end
  else
  begin
    edtToken.Text := EmptyStr;
  end;
end;

end.
