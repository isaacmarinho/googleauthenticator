object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'GoogleAuthenticator'
  ClientHeight = 206
  ClientWidth = 357
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object edtSerial: TEdit
    Left = 184
    Top = 24
    Width = 121
    Height = 21
    ParentShowHint = False
    PasswordChar = '*'
    ShowHint = True
    TabOrder = 0
    TextHint = 'Serial'
    OnChange = edtSerialChange
  end
  object btnOk: TButton
    Left = 136
    Top = 80
    Width = 75
    Height = 30
    Caption = 'Ok'
    Enabled = False
    TabOrder = 1
    OnClick = btnOkClick
  end
  object edtPassword: TEdit
    Left = 41
    Top = 24
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    TextHint = 'Password'
    OnChange = edtPasswordChange
  end
  object edtToken: TEdit
    Left = 41
    Top = 136
    Width = 264
    Height = 37
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    TextHint = 'Left click to copy!'
  end
  object tAuth: TTimer
    OnTimer = tAuthTimer
    Left = 232
    Top = 80
  end
  object appevMain: TApplicationEvents
    OnShortCut = appevMainShortCut
    Left = 32
    Top = 72
  end
end
